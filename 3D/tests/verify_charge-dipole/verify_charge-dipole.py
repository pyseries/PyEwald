#!/usr/bin/python3
from math import atan
import cmath

"""
Perform dipole-charge interaction in real-space directly to compare
to the Ewald summation.

The Ewald result is 4.7172915500549095 (2x2x2 superlattice).

The following calculation results in:
      N            result
     20 4.883778724576097
     40 4.774167444523269
    100 4.726810860288341
    200 4.733711241875332

Note in the following calculation, it is important to keep "charge neutral" in
the summation (Ref. our paper). To this end, we introduced the N_cut to make
sure the summation is performed within a sphere, where charge and dipole are
neutral.

"""

N = 20
N_cut = N
pi = 4.0*atan(1.0)
pi2 = pi*2.0

kp=(0,0,1)

delung = 0.0
for i in range(-N,N):
    for j in range(-N,N):
        for k in range(-N,N):
            # Set the value of p on each site.
            dum = cmath.exp(1j * pi * (kp[0]*i + kp[1]*j + kp[2]*k) )
            p = dum.real # The value is +1 or -1.
            # Now calculate the interaciton between dipole and charge.
            # Remember charge is at (1/2,1/2,1/2)
            r = (0.5-i,0.5-j,0.5-k)
            r_mag = (r[0]**2+r[1]**2+r[2]**2)**0.5
            if r_mag > N_cut:
                continue
            delung += p*r[2]/r_mag**3

print(delung)


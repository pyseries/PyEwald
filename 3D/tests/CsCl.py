#!/usr/bin/python3

"""
.. module:: CsCl
    :synopsis: Obtain the Madelung constant for CsCl using the charge-charge interaction matrix.

.. moduleauthor:: D. Wang <dwang5@zoho.com>

.. note::
   The value calculated here is per "ion". For :math:`N` pairs of ions,

   .. math::
      U_{\\textrm{coulomb}} = 2 N M_0 e^2/(4\pi\\varepsilon_0)

   where :math:`2M_0` is the usual Madelung constant.

For CsCl, the value is  :math:`M_0=-0.8813`, see The Journal of Chemical Physics, **34**, 2004 (1961).
Also see pp 40 of "Solid State Physics" by Lu Dong; Jiang Ping; Xu Zhizhong.


"""

import sys
import os
sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from charge_charge import Charge_charge
from dipole_dipole import Dip_dip
from configuration2D import *
from utility import *
from math import sqrt


n1 = 2
n2 = 2
n3 = 2

# ----- CsCl -----
lattice = np.array([[-1,1,1],[1,-1,1],[1,1,-1]])/sqrt(3.0)
nc_file = "CsCl-matrix_{n1}x{n2}x{n3}.nc".format(n1=n1,n2=n2,n3=n3)
calculated = os.path.exists(nc_file)
if (not calculated):
    cc = Charge_charge(n1,n2,n3,lattice)
    cc.write_charge_matrix(nc_file)

#Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
#print(dataset.dimensions.keys())
#print(dataset.dimensions['ia'])
cc_mat = dataset['matrix']

g_alloy = Charge_config(n1,n2,n3,lattice)
g_alloy.generate_high_symmetry(p=1,k=(1,1,1))
alloy_fn = "CsCl_config.nc"
g_alloy.write_alloy_matrix(alloy_fn)
# The called function is defined in 'utility.py'.
rslt = calc_madelung_cc(cc_mat,alloy_fn)

print("Madelung constant (CsCl): ", rslt)

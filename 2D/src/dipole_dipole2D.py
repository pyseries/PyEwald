"""
.. module:: Dip_Dip
    :synopsis: Generate the Dipole_dipole interaction matrix.

.. moduleauthor:: D. Wang <dwang5@zoho.com>
"""
from math import *
import numpy as np
from netCDF4 import Dataset
import time

from numba import *
# from utility import *
from supercell import Supercell

@njit(float64[:](float64[:, :], float64, int64, int64, float64, float64, float64))
def dd_sum_over_k(h, eta, mg1, mg2, rx, ry, rz):
    dumPtr = np.zeros(6)
    for ig1 in range(0, mg1 + 1):
        for ig2 in range(-mg2, mg2 + 1):
            gx = ig1 * h[0, 0] + ig2 * h[1, 0]
            gy = ig1 * h[0, 1] + ig2 * h[1, 1]
            g = sqrt(gx * gx + gy * gy)
            if g > 1.0e-8:
                factor = 1.0
                if ig1 == 0:
                    factor = 0.5
                # Suppose h_z is zero (i.e., gz==0 here).
                cc = cos(gx * rx + gy * ry)
                ss = sin(gx * rx + gy * ry)

                # First line of the expression
                dum0 = factor * cc / g * \
                       (exp(g * rz) * erfc(eta * rz + g / (2.0 * eta)) +
                        exp(-g * rz) * erfc(-eta * rz + g / (2.0 * eta)))
                dumPtr[0] += dum0 * gx * gx
                dumPtr[1] += dum0 * gy * gy
                # For axis related to z all are zero.
                # dumPtr[2] += dum0 * gz * gz
                # dumPtr[3] += dum0 * gz * gy
                # dumPtr[4] += dum0 * gx * gz
                dumPtr[5] += dum0 * gx * gy

                # Second line of the expression.
                dum1 = factor * g * cc * exp(rz * g) * erfc(eta * rz + g / (2.0 * eta))
                dumPtr[2] -= dum1

                # Third line of the expression.
                dum2 = factor * ss / g * exp(rz * g) * erfc(eta * rz + g / (2.0 * eta))
                # dumPtr[2] -= 2.0 * gz * dum2 # but gz ==0, so nothing to be done here.
                dumPtr[3] -= gy * dum2
                dumPtr[4] -= gx * dum2

    return 2.0 * dumPtr

class Dip_dip2D(Supercell):
    """
    The parameters are similar to that of 'charge-charge'.

    """
    def __init__(self, n1, n2, nz, lattice):
        Supercell.__init__(self, n1, n2, nz, lattice)
        self.dpij = np.zeros((self.nsites, 6))
        self.dipole_matrix_calculated = False

    def write_dipole_matrix(self, fn):
        if self.dipole_matrix_calculated == False:
            print("Calculate the matrix first ...")
            self.generate_dipole_matrix()
            self.dipole_matrix_calculated = True

        dipm = Dataset(fn, "w", format="NETCDF4")
        direction = dipm.createDimension("direction", 6)
        ia = dipm.createDimension("ia", None)
        directions = dipm.createVariable("direction", np.int32, ("direction"))
        ias = dipm.createVariable("ia", np.int32, ("ia"))
        # The actual 2-d varable.
        matrix = dipm.createVariable('matrix', np.float64, ('ia', 'direction'))

        dipm.description = 'Dipole matrix: interaction matrix'
        dipm.history = 'Created at ' + time.ctime(time.time())
        matrix[:, :] = self.dpij
        dipm.close()

    def generate_dipole_matrix(self):
        pi = 4.0 * atan(1.0)
        pi2 = pi * 2.0

        aa = [0.0, 0.0, 0.0]
        for i in range(3):
            aa[i] = np.linalg.norm(self.lattice[i])
        a0 = min(aa)

        tol = 1.0e-12
        eta = sqrt(-log(tol)) / a0
        gcut = 2.0 * eta ** 2
        gcut2 = gcut ** 2
        eta4 = 1.0 / (4 * eta ** 2)

        am = np.zeros(3)
        for i in range(3):
            for k in range(3):
                am[i] += self.a[k, i] ** 2
            am[i] = sqrt(am[i])

        mg1 = int(gcut * am[0] / pi2) + 1
        mg2 = int(gcut * am[1] / pi2) + 1
        mg3 = int(gcut * am[2] / pi2) + 1
        print('Gcut: ', gcut, ' mg1, mg2, mg3: ', mg1, mg2, mg3)

        pos0 = self.ixa[0] * self.lattice[0, :] \
               + self.iya[0] * self.lattice[1, :] \
               + self.iza[0] * self.lattice[2, :]

        for ia in range(self.nsites):
            print('site: ', ia)
            pos = np.zeros(3)
            pos = self.ixa[ia] * self.lattice[0, :] \
                  + self.iya[ia] * self.lattice[1, :] \
                  + self.iza[ia] * self.lattice[2, :]
            rx = pos[0]
            ry = pos[1]
            rz = pos[2]

            origin = (rx == 0) and (ry == 0) and (rz == 0)
            c = pi / (2.0 * self.A)
            residue = 2.0 * eta ** 3 / (3.0 * sqrt(pi))

            dum = dd_sum_over_k(self.h, eta, mg1, mg2, rx - pos0[0], ry - pos0[1], rz - pos0[2])

            # The 2.0 comes from how the sum over k was done.
            self.dpij[ia, :] = dum[:] * c
            if origin:
                self.dpij[ia, 0:2] -= residue

            term2 = 2 * sqrt(pi) / self.A * (2 * rz ** 2 * eta ** 2 - 1) * eta * exp(-rz ** 2 * eta ** 2)
            self.dpij[ia, 2] += term2

        self.dipole_matrix_calculated = True

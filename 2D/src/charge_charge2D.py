"""
.. module:: Charge_charge2D
    :synopsis: Generate the charge_charge2D interaction matrix.
    :reference: J. Chem. Phys. 111, 3155 (1999); doi: 10.1063/1.479595

.. moduleauthor:: D. Wang <dwang5@zoho.com>
"""

import numpy as np
from numba import *
from netCDF4 import Dataset
import time
from math import atan, sqrt, log, cos, exp, erfc, erf
from supercell import Supercell


@njit(float64(float64[:, :], float64, int64, int64, float64, float64, float64))
def cc_sum_over_k(h, eta, mg1, mg2, rx, ry, rz):
    dum = 0.0
    for ig1 in np.arange(0, mg1 + 1):
        for ig2 in np.arange(-mg2, mg2 + 1):
            gx = ig1 * h[0, 0] + ig2 * h[1, 0]
            gy = ig1 * h[0, 1] + ig2 * h[1, 1]
            g = sqrt(gx * gx + gy * gy)
            if g > 1.0e-8:
                factor = 1.0
                if ig1 == 0:
                    factor = 0.5
                dum += factor * cos(gx * rx + gy * ry) / g * \
                       (exp(g * rz) * erfc(eta * rz + g / (2.0 * eta)) +
                        exp(-g * rz) * erfc(-eta * rz + g / (2.0 * eta)))
    return 2.0 * dum


@njit(float64(float64[:, :], float64, int64, float64, float64, float64))
def cc_sum_over_r(a, eta, NN, rx, ry, rz):
    dum = 0.0
    for ir1 in range(-NN, NN + 1):
        for ir2 in range(-NN, NN + 1):
            if ir1 == 0 and ir2 == 0 and rx == ry == rz == 0:
                continue
            Rpos = ir1 * a[0, :] + ir2 * a[1, :]
            Rix = Rpos[0]
            Riy = Rpos[1]
            Riz = Rpos[2]
            x = (rx + Rix)
            y = (ry + Riy)
            z = (rz + Riz)
            r = sqrt(x ** 2 + y ** 2 + z ** 2)
            dum += 1.0 / r * erfc(r * eta)

    return dum


class Charge_charge2D(Supercell):
    """
    Charge_charge2D inherits the *Supercell* class, it first initializes the supercell it works on.

    :param n1: Number of unit cells along the first Bravais vector of 'lattice'.
    :param n2: Number of unit cells along the second Bravais vector of 'lattice'.
    :param nz: Number of unit cells along the third Bravais vector of 'lattice',There's no periodicity in this direction
    :param lattice: The lattice of the **unit cell**, not the supercell.
    """

    def __init__(self, n1, n2, nz, lattice):
        Supercell.__init__(self, n1, n2, nz, lattice)
        self.ccij = np.zeros(self.nsites)
        self.charge_matrix_calculated = False

    def write_charge_matrix(self, fn):
        """
        Write the calculated interaction matrix to a netcdf file for later us.

        :param fn: The name of the netcdf file.
        :return: Nothing.
        """
        if not self.charge_matrix_calculated:
            print("Calculate the matrix first ...")
            self.generate_charge_matrix()
            self.charge_matrix_calculated = True

        ccm = Dataset(fn, "w", format="NETCDF4")
        ia = ccm.createDimension("ia", None)
        # ias = ccm.createVariable("ia",np.int32,("ia"))
        # The actual 2-d varable.
        matrix = ccm.createVariable('matrix', np.float64, 'ia')

        ccm.description = 'Charge matrix: interaction matrix'
        ccm.history = 'Created at ' + time.ctime(time.time())
        matrix[:] = self.ccij[:]
        ccm.close()

    def generate_charge_matrix(self):
        """
        This is the core of the interaction matrix calculation.

        'cc_sum_over_k' is to sum over the k space, the long range part.
        'cc_sum_over_r' is to sum over the real space, the short range part.
        :return: nothing.
        """
        pi = 4.0 * atan(1.0)
        pi2 = pi * 2.0
        # NN sets how many unit cells will be used for the real-space sum.
        # As explained in our paper, real-space sum can be ignored.
        NN = 10
        aa = [0.0, 0.0, 0.0]
        for i in range(3):
            aa[i] = np.linalg.norm(self.lattice[i])
        a0 = min(aa)

        tol = 1.0e-12
        """
        eta seems to be very important, especially in the z direction.
        It needs to take a proper value, so that the real part might work, and I need to go through the real part.
        """
        eta = sqrt(-log(tol)) / a0
        gcut = 2.0 * eta ** 2
        eta4 = 1.0 / (4 * eta ** 2)

        am = np.zeros(3)
        for i in range(3):
            for k in range(3):
                am[i] += self.a[i, k] ** 2
            am[i] = sqrt(am[i])

        mg1 = int(gcut * am[0] / pi2) + 1
        mg2 = int(gcut * am[1] / pi2) + 1
        mg3 = int(gcut * am[2] / pi2) + 1
        print('Gcut: ', gcut, ' mg1, mg2, mg3: ', mg1, mg2, mg3)

        c = pi / (2.0 * self.A)
        residue = eta / sqrt(pi)

        pos0 = np.zeros(3)
        pos0 = self.ixa[0] * self.lattice[0, :] \
               + self.iya[0] * self.lattice[1, :] \
               + self.iza[0] * self.lattice[2, :]
        print("pos0", pos0)
        for ia in range(self.nsites):
            # Note how the three dimensional (dx,dy,dz) is mapped
            # into a single array, which is important for correct
            # later use of the generated matrix.
            print('site: ', ia)
            pos = np.zeros(3)
            pos = self.ixa[ia] * self.lattice[0, :] \
                  + self.iya[ia] * self.lattice[1, :] \
                  + self.iza[ia] * self.lattice[2, :]
            rx = pos[0]
            ry = pos[1]
            rz = pos[2]

            # Formula 6, the first term.It is not necessary, the return value is 0.
            rrslt = cc_sum_over_r(self.a, eta, NN, rx - pos0[0], ry - pos0[1], rz - pos0[2])
            self.ccij[ia] += rrslt * 0.5

            # Formula 6, The second term.
            krslt = cc_sum_over_k(self.h, eta, mg1, mg2, rx - pos0[0], ry - pos0[1], rz - pos0[2])
            self.ccij[ia] += krslt * c

            # Formula 6. The third term.
            zij = rz - pos0[2]
            self.ccij[ia] -= pi / self.A * (zij * erf(eta * zij) + 1 / (eta * sqrt(pi)) * exp(-(eta * zij) ** 2))

            # Formula 6, the last term.
            if ia == 0:
                self.ccij[ia] -= residue
